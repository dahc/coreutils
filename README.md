# Redox OS coreutils

This repository contains the core UNIX utilities for Redox OS. These are based on BSD coreutils rather than GNU coreutils as these tools should be as minimal as possible.

**Currently included:**

- `cat`
- `chmod`
- `clear`
- `dd`
- `df`
- `du`
- `env`
- `free`
- `kill`
- `ln`
- `mkdir`
- `ps`
- `reset`
- `shutdown`
- `sort`
- `stat`
- `tail`
- `tee`
- `test`
- `time`
- `touch`
- `uname`
- `uptime`
- `which`
